/*
 * Driving.cpp
 *
 * Created: 10/11/2019 5:15:46 AM
 *  Author: huynh
 */ 

#include "Driving.h"
#include "BitOperations.h"
#include "Uart.h"
#include "Instructions.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define PI 3.1415926535897932384626433832795
const double pulsesPerMilimeter = (MOTOR_PULSE_PER_ROUND / GEAR_RATIO) / (WHEEL_DIAMETER_MILIMETER * PI);


void initDriver(Uart& uart) {
	/************************************************************************/
	/* Setup motor pins as output                                           */
	/************************************************************************/
	// Left motor control pin
	SET_IO_DIR_OUTPUT(LEFT_MOTOR_PULSE);		// Set IO output
	SET_IO_DIR_OUTPUT(LEFT_MOTOR_EN);			// Set IO output
	SET_IO_DIR_OUTPUT(LEFT_MOTOR_DIR);			// Set IO output
	// Right motor control pin
	SET_IO_DIR_OUTPUT(RIGHT_MOTOR_PULSE);		// Set IO output
	SET_IO_DIR_OUTPUT(RIGHT_MOTOR_EN);			// Set IO output
	SET_IO_DIR_OUTPUT(RIGHT_MOTOR_DIR);			// Set IO output
	// Power control pin
	SET_IO_DIR_OUTPUT(DRIVER_POWER_SOURCE);		// Set IO output
	SET_IO_DIR_OUTPUT(COLLECTOR_MOTOR_PIN);		// Set IO output	
	SET_IO_DIR_OUTPUT(LIGHT_PIN);				// SET IO output
	// Start
	IO_WRITE_LOW(LEFT_MOTOR_EN);				// Enable left motor
	IO_WRITE_LOW(RIGHT_MOTOR_EN);				// Enable right motor
	IO_WRITE_HIGH(DRIVER_POWER_SOURCE);			// Power on the driver
	IO_WRITE_LOW(COLLECTOR_MOTOR_PIN);			// Turn on the ball collector
	IO_WRITE_LOW(LIGHT_PIN);					// Turn on the light
	/************************************************************************/
	/* Setup the timer 1 to CTC mode and enable interrupt                   */
	/************************************************************************/
	SET_BIT3(TCCR1B);	// Set the CTC mode
	SET_BIT1(TIMSK1);	// Enable output compare match A interrupt
	SET_BIT2(TIMSK1);	// Enable output compare match B interrupt
	/************************************************************************/
	/* Setup the timer 3 to CTC mode and enable interrupt                   */
	/************************************************************************/
	SET_BIT3(TCCR3B);	// Set the CTC mode
	SET_BIT1(TIMSK3);	// Enable output compare match A interrupt
	SET_BIT2(TIMSK3);	// Enable output compare match B interrupt
	/************************************************************************/
	/* Set the initial value and start timers                               */
	/************************************************************************/
	sei();				// Allow global interrupt
	TCNT1 = 0;			// Initialize the first value of the timer 1
	TCNT3 = 0;			// Initialize the first value of the timer 3
	CLEAR_BIT3(PRR0);	// Turn off the power reduction for timer 1, so it can operate
	CLEAR_BIT3(PRR1);	// Turn off the power reduction for timer 3, so it can operate
	/************************************************************************/
	/* Set up for getting data from an uart                                 */
	/************************************************************************/
	uart.setMsgRecvBuffer(instructionBuffer, INSTRUCTION_BUFFER_SIZE);
	uart.setCallBack([](Uart& _uart) {
		int status = _uart.readMsg();
		Instruction instruction(instructionBuffer);
		instruction.execute();
		_uart.write(instructionBuffer, status);
	});
}

void setLightLevel(long percent) {
	if (percent > 100 || percent < 0) return;
	if (percent == 0) {
		CLEAR_BIT0(TCCR5B);
		IO_WRITE_LOW(LIGHT_PIN);
	}
	else if (percent == 100) {
		CLEAR_BIT0(TCCR5B);
		IO_WRITE_HIGH(LIGHT_PIN);
	}
	else {
		TCNT5 = 0;
		OCR5A = ((double)percent/100) * 0xFFFF;
		SET_BIT0(TCCR5B);
		SET_BIT0(TIMSK5);
		SET_BIT1(TIMSK5);
		sei();
	}
}
void setCollectorSpeed(long percent) {
	if (percent > 100 || percent < 0) return;
	if (percent == 0) {
		CLEAR_BIT0(TCCR4B);
		IO_WRITE_LOW(COLLECTOR_MOTOR_PIN);
	}
	else if (percent == 100) {
		CLEAR_BIT0(TCCR4B);
		IO_WRITE_HIGH(COLLECTOR_MOTOR_PIN);
	}
	else {
		TCNT4 = 0;
		OCR4A = ((double)percent/100) * 0xFFFF;
		SET_BIT0(TCCR4B);
		SET_BIT0(TIMSK4);
		SET_BIT1(TIMSK4);
		sei();
	}
}

long leftMotorPulsesPerSecond;
long leftMotorNumberOfPulses;
long leftMotorCycleTimeMicrosecond;
// long leftMotorCycleLowTimeMicrosecond;

void runLeft(long distanceMilimeter, long speedMilimeterPerSecond) {
	leftMotorPulsesPerSecond = pulsesPerMilimeter * speedMilimeterPerSecond;
	leftMotorNumberOfPulses = distanceMilimeter * pulsesPerMilimeter;
	leftMotorCycleTimeMicrosecond = 1000000.0 / leftMotorPulsesPerSecond;
	leftMotorPulseCount = 0;
	OCR1A = 2 * leftMotorCycleTimeMicrosecond;
	OCR1B = 2 * CYCLE_HIGH_TIME_MICROSECOND;
	SET_BIT1(TCCR1B);			// Start the timer with prescaler 8
}

long rightMotorPulsesPerSecond;
long rightMotorNumberOfPulses;
long rightMotorCycleTimeMicrosecond;

void runRight(long distanceMilimeter, long speedMilimeterPerSecond) {
	rightMotorPulsesPerSecond = pulsesPerMilimeter * speedMilimeterPerSecond;
	rightMotorNumberOfPulses = distanceMilimeter * pulsesPerMilimeter;
	rightMotorCycleTimeMicrosecond = 1000000.0 / rightMotorPulsesPerSecond;
	rightMotorPulseCount = 0;
	OCR3A = 2 * rightMotorCycleTimeMicrosecond;
	OCR3B = 2 * CYCLE_HIGH_TIME_MICROSECOND;
	SET_BIT1(TCCR3B);			// Start the timer with prescaler 8
}

void stop() {
	// The timer's ISR will stop the timer if there's no remaining task
	leftMotorNumberOfPulses = 0;
	rightMotorNumberOfPulses = 0;
}

void cutDriverPower() {	IO_WRITE_LOW(DRIVER_POWER_SOURCE); }

void goStraight(long _distance) {
	if (_distance == 0) return;
	if (LEFT_MOTOR_DIR_REVERSED ? _distance < 0 : _distance > 0)
		IO_WRITE_HIGH (LEFT_MOTOR_DIR);
	else IO_WRITE_LOW (LEFT_MOTOR_DIR);
	if (RIGHT_MOTOR_DIR_REVERSED ? _distance < 0 : _distance > 0)
		IO_WRITE_HIGH (RIGHT_MOTOR_DIR);
	else IO_WRITE_LOW (RIGHT_MOTOR_DIR);
	_distance = _distance > 0 ? _distance : -_distance;
	runLeft(_distance, MAX_ROBOT_SPEED_MILIMETER_PER_SEC);
	runRight(_distance, MAX_ROBOT_SPEED_MILIMETER_PER_SEC);
}


void rotate(float _angle) {
	if (_angle == 0) return;
	long distance = (_angle/360.0) * DISTANCE_BETWEEN_TWO_WHEEL_MILIMETER * PI;
	if (LEFT_MOTOR_DIR_REVERSED ? _angle < 0 : _angle > 0)
		IO_WRITE_HIGH (LEFT_MOTOR_DIR);
	else IO_WRITE_LOW (LEFT_MOTOR_DIR);
	if (RIGHT_MOTOR_DIR_REVERSED ? _angle > 0 : _angle < 0)
		IO_WRITE_HIGH (RIGHT_MOTOR_DIR);
	else IO_WRITE_LOW (RIGHT_MOTOR_DIR);
	distance = distance > 0 ? distance : -distance;
	runLeft(distance, MAX_ROBOT_SPEED_MILIMETER_PER_SEC);
	runRight(distance, MAX_ROBOT_SPEED_MILIMETER_PER_SEC);
}

void runArc(unsigned char _side, long _radius, float _angle) {
	if (_radius < DISTANCE_BETWEEN_TWO_WHEEL_MILIMETER / 2.0) return;
	if (_angle == 0) return;
	if (LEFT_MOTOR_DIR_REVERSED ? _angle < 0 : _angle > 0)
		IO_WRITE_HIGH (LEFT_MOTOR_DIR);
	else IO_WRITE_LOW (LEFT_MOTOR_DIR);
	if (RIGHT_MOTOR_DIR_REVERSED ? _angle < 0 : _angle > 0)
		IO_WRITE_HIGH (RIGHT_MOTOR_DIR);
	else IO_WRITE_LOW (RIGHT_MOTOR_DIR);
	long innerDistance = (_angle/180.0) * PI * (_radius - DISTANCE_BETWEEN_TWO_WHEEL_MILIMETER / 2.0);
	long outerDistance = (_angle/180.0) * PI * (_radius + DISTANCE_BETWEEN_TWO_WHEEL_MILIMETER / 2.0);
	innerDistance = innerDistance > 0 ? innerDistance : -innerDistance;
	outerDistance = outerDistance > 0 ? outerDistance : -outerDistance;
	long innerSpeed = MAX_ROBOT_SPEED_MILIMETER_PER_SEC * (innerDistance/((double)outerDistance));
	runLeft(_side == 0 ? innerDistance : outerDistance,
			_side == 0 ? innerSpeed : MAX_ROBOT_SPEED_MILIMETER_PER_SEC);
	runRight(_side == 0 ? outerDistance : innerDistance,
			 _side == 0 ? MAX_ROBOT_SPEED_MILIMETER_PER_SEC : innerSpeed);
}