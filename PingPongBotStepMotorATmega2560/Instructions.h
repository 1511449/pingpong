// Instructions.h

#ifndef _INSTRUCTIONS_h
#define _INSTRUCTIONS_h

#define INSTRUCTION_SIGNATURE		0x0509
#define INSTRUCTION_BUFFER_SIZE		64

extern char instructionBuffer[INSTRUCTION_BUFFER_SIZE];

enum InstructionType {
	EMER_STOP			= 0x00,
	STOP				= 0x01,
	RUN_STRAIGHT		= 0x02,
	ROTATE				= 0x03,
	RUN_ARC				= 0x04,
	SET_COLL_SP			= 0x05,
	SET_LIGHT			= 0x06,
	BATT_REQ			= 0x07,
	BATT_RES			= 0x08,
	BATT_WARN			= 0x09,
	OBSTACLES_WARN		= 0x0A,
	UNKNOWN_ERR			= 0x0B
};

class Instruction {
public:
	Instruction(char instructionBuffer[]);
	void execute();
private:
	InstructionType type;
	void(*callback)(void* param1, void* param2);
};

#endif

