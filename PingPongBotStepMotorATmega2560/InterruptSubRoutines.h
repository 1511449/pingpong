/*
 * InterruptSubRoutines.h
 *
 * Created: 10/11/2019 5:06:41 AM
 *  Author: huynh
 */ 


#ifndef INTERRUPTSUBROUTINES_H_
#include "BitOperations.h"
#define INTERRUPTSUBROUTINES_H_

extern volatile long leftMotorPulseCount;
extern volatile long rightMotorPulseCount;

#endif /* INTERRUPTSUBROUTINES_H_ */