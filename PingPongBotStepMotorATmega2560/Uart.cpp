/*
 * SerialCommunication.cpp
 *
 * Created: 12/11/2019 12:49:36 AM
 *  Author: huynh
 */ 

#include "Uart.h"
#include "BitOperations.h"
#include <math.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

/************************************************************************/
/* UART object constructor                                              */
/************************************************************************/
Uart::Uart(int _uartNo):uartNo(_uartNo), recvBuffer(NULL), recvBufferSize(0) {};
	
/************************************************************************/
/* Define a macro for 4 uarts initialization                            */
/************************************************************************/
	
#define INIT_UART(uartNo) 						\
SET_BIT7(UCSR##uartNo##B);						/* Allow RX complete interrupt */\
SET_BIT4(UCSR##uartNo##B);						/* Enable RX */\
SET_BIT3(UCSR##uartNo##B);						/* Enable TX */\
UCSR##uartNo##C &= 0b00111111;					/* Set asynchronous mode */\
CLEAR_BIT2(UCSR##uartNo##B);					\
switch (charactersize) {						/* Set the character size */\
	case 5:										/* 5 bits of data per frame */\
		UCSR##uartNo##C &= 0b11111001;			\
		break;									\
	case 6:										/* 6 bits of data per frame */\
		CLEAR_BIT2(UCSR##uartNo##C);			\
		SET_BIT1(UCSR##uartNo##C);				\
		break;									\
	case 7:										/* 7 bits of data per frame */\
		CLEAR_BIT1(UCSR##uartNo##C);			\
		SET_BIT2(UCSR##uartNo##C);				\
		break;									\
	case 8:										/* 8 bits of data per frame */\
		UCSR##uartNo##C |= 0b00000110;			\
		break;									\
	default:									\
		break;									\
}												\
switch (parity) {								/* Set if the parity is included or not */\
	case NO_PARITY:								/* Frame format without parity bit */\
		UCSR##uartNo##C &= 0b11001111;			\
		break;									\
	case ODD_PARITY:							/* Frame format with odd parity bit */\
		UCSR##uartNo##C |= 0b00110000;			\
		break;									\
	case EVEN_PARITY:							/* Frame format with even parity bit */\
		SET_BIT5(UCSR##uartNo##C);				\
		CLEAR_BIT4(UCSR##uartNo##C);			\
		break;									\
	default:									\
		break;									\
}															\
if (numberOfStopBit == 0) CLEAR_BIT3(UCSR##uartNo##C);		\
else if (numberOfStopBit == 2) SET_BIT3(UCSR##uartNo##C);	\
if (doubleSpeed) {											\
	ubrr_value = UBBR_VALUE_DOUBLE_SPEED(baudrate);			\
	SET_BIT1(UCSR##uartNo##A);								\
}															\
else {														\
	CLEAR_BIT1(UCSR##uartNo##A);							\
	ubrr_value = UBBR_VALUE_NORMAL(baudrate);				\
}															\
UBRR##uartNo##H = (unsigned char)( ubrr_value >> 8);		\
UBRR##uartNo##L = (unsigned char)ubrr_value;				\
	
/************************************************************************/
/* UART begin() method using above macro                                */
/************************************************************************/	
	
void Uart::begin(long baudrate, int charactersize, unsigned char parity, unsigned char numberOfStopBit, bool doubleSpeed) {
	int ubrr_value;	
	switch (this->uartNo) {
		case 0:
			CLEAR_BIT1(PRR0);	// Turn off power saving mode for uart module
			CLEAR_BIT0(DDRE);	// Set RXD0 as input
			SET_BIT1(DDRE);		// Set TXD0 as output
			INIT_UART(0);
			break;
		case 1:
			CLEAR_BIT0(PRR1);	// Turn off power saving mode for uart module
			CLEAR_BIT2(DDRD);	// Set RXD1 as in put
			SET_BIT3(DDRD);		// Set TXD1 as output
			INIT_UART(1);
			break;
		case 2:
			CLEAR_BIT1(PRR1);	// Turn off power saving mode for uart module
			CLEAR_BIT0(DDRH);	// Set RXD2 as in put
			SET_BIT1(DDRH);		// Set TXD2 as output
			INIT_UART(2);
			break;
		case 3:
			CLEAR_BIT2(PRR1);	// Turn off power saving mode for uart module
			CLEAR_BIT0(DDRJ);	// Set RXD3 as in put
			SET_BIT1(DDRJ);		// Set TXD3 as output
			INIT_UART(3);
			break;
		default:
			break;
	}
	sei();
}		

/************************************************************************/
/* UART write method                                                    */
/************************************************************************/	

#define DEF_UART_SEND_BYTE(uartNo)					\
	void uart##uartNo##Send (unsigned char data) {	\
		while(!BIT5_IS_SET(UCSR##uartNo##A));		\
		UDR##uartNo = data;							\
	}												\

DEF_UART_SEND_BYTE(0)
DEF_UART_SEND_BYTE(1)
DEF_UART_SEND_BYTE(2)
DEF_UART_SEND_BYTE(3)

#define SEND_UART(uartNo, data) uart##uartNo##Send(data)
void Uart::write(char* const buffer, int _numberOfBytes) {
	int numberOfBytes = _numberOfBytes > 0 ? _numberOfBytes : strlen(buffer);
	switch (this->uartNo) {
		case 0:
			for (int i = 0; i < numberOfBytes; i++) SEND_UART(0, buffer[i]);
			break;
		case 1:
			for (int i = 0; i < numberOfBytes; i++) SEND_UART(1, buffer[i]);
			break;
		case 2:
			for (int i = 0; i < numberOfBytes; i++) SEND_UART(2, buffer[i]);
			break;
		case 3:
			for (int i = 0; i < numberOfBytes; i++) SEND_UART(3, buffer[i]);
			break;
		default:
			break;
	}
}

#define SEND_CR_LF(uartNo) SEND_UART(uartNo, '\r'); SEND_UART(uartNo, '\n');
void Uart::writeln(char* const buffer, int _numberOfBytes) {
	this->write(buffer, _numberOfBytes);
	switch (this->uartNo) {
		case 0:
			SEND_CR_LF(0)
			break;
		case 1:
			SEND_CR_LF(1)
			break;
		case 2:
			SEND_CR_LF(2)
			break;
		case 3:
			SEND_CR_LF(3)
			break;
		default:
			break;
	}
}
/************************************************************************/
/* UART ISR for receiving                                               */
/************************************************************************/
#define DEF_UART_READ_ON_ISR(uartNo)			\
	unsigned char uart##uartNo##ReadOnISR () {	\
		while (!BIT7_IS_SET(UCSR##uartNo##A));	\
		return UDR##uartNo;						\
	}											\

DEF_UART_READ_ON_ISR(0)
DEF_UART_READ_ON_ISR(1)
DEF_UART_READ_ON_ISR(2)
DEF_UART_READ_ON_ISR(3)

#define DECLARE_UART_RECV_QUEUE(uartNo)														\
	volatile char uart##uartNo##RecvBuffer[RECV_BUFFER_SIZE];								\
	volatile int uartFirst##uartNo = 0, uartLast##uartNo = -1, uartAvailable##uartNo = 0;	\
	volatile bool isAboutToStopUart##uartNo = false;										\
	void (* volatile uart##uartNo##CRLFcallback)(Uart& _uart) = [](Uart& _uart){}\

DECLARE_UART_RECV_QUEUE(0);
DECLARE_UART_RECV_QUEUE(1);
DECLARE_UART_RECV_QUEUE(2);
DECLARE_UART_RECV_QUEUE(3);

#define DEF_RX_ISR_UART(uartNo)																					\
	ISR(USART##uartNo##_RX_vect) {																				\
		if(uartAvailable##uartNo < RECV_BUFFER_SIZE) {					/* If buffer is not full */				\
			uartLast##uartNo = (uartLast##uartNo + 1) % RECV_BUFFER_SIZE;	/* Calculate the next pos */		\
			char c = (uart##uartNo##RecvBuffer[uartLast##uartNo] = uart##uartNo##ReadOnISR());	/* read data */	\
			uartAvailable##uartNo++;					/* Update the number of available data */				\
			if (c == '\r') isAboutToStopUart##uartNo = true;													\
			else if (c != '\n') isAboutToStopUart##uartNo = false;												\
			else if (c == '\n' && isAboutToStopUart##uartNo) {													\
				uart##uartNo##CRLFcallback(Uart##uartNo);														\
				isAboutToStopUart##uartNo = false;																\
			}																									\
		}																										\
	}																											\
	
DEF_RX_ISR_UART(0);
DEF_RX_ISR_UART(1);
DEF_RX_ISR_UART(2);
DEF_RX_ISR_UART(3);

/************************************************************************/
/* Methods for reading uart data and do some task                       */
/************************************************************************/

void Uart::setMsgRecvBuffer(char* const _recvBuffer, const int _recvBufferSize) {
	this->recvBuffer = _recvBuffer;
	this->recvBufferSize = _recvBufferSize;
}

#define SET_CALLBACK_UART(uartNo) uart##uartNo##CRLFcallback = _callback
void Uart::setCallBack(void(*_callback)(Uart&)) {
	switch(this->uartNo) {
		case 0:
			SET_CALLBACK_UART(0);
			break;
		case 1:
			SET_CALLBACK_UART(1);
			break;
		case 2:
			SET_CALLBACK_UART(2);
			break;
		case 3:
			SET_CALLBACK_UART(3);
			break;
		default:
			break;
	}
}

#define DATA_AVAILABEL_UART(uartNo)	uartAvailable##uartNo
int Uart::available() {
	switch(this->uartNo) {
		case 0:
			return DATA_AVAILABEL_UART(0);
			break;
		case 1:
			return DATA_AVAILABEL_UART(1);
			break;
		case 2:
			return DATA_AVAILABEL_UART(2);
			break;
		case 3:
			return DATA_AVAILABEL_UART(3);
			break;
		default:
			break;
	}
	return 0;
}

#define DEQUEUE_ONE_CHAR_FROM_UART(uartNo)								\
	{																	\
		temp = uart##uartNo##RecvBuffer[uartFirst##uartNo];				\
		uartFirst##uartNo = (uartFirst##uartNo + 1) % RECV_BUFFER_SIZE;	\
		uartAvailable##uartNo--;										\
	}																	\

char Uart::readChar() {
	char temp = 0;
	switch(this->uartNo) {
		case 0:
			if (DATA_AVAILABEL_UART(0)) DEQUEUE_ONE_CHAR_FROM_UART(0);
			break;
		case 1:
			if (DATA_AVAILABEL_UART(1)) DEQUEUE_ONE_CHAR_FROM_UART(1);
			break;
		case 2:
			if (DATA_AVAILABEL_UART(2)) DEQUEUE_ONE_CHAR_FROM_UART(2);
			break;
		case 3:
			if (DATA_AVAILABEL_UART(3)) DEQUEUE_ONE_CHAR_FROM_UART(3);
			break;
		default:
			break;
	}
	return temp;
}


#define READ_MSG_UART(uartNo)															\
	while(uartAvailable##uartNo > 0 && index < this->recvBufferSize) {					\
		char temp;																		\
		DEQUEUE_ONE_CHAR_FROM_UART(uartNo);												\
		this->recvBuffer[index++] = temp;												\
	}																					\


int Uart::readMsg() {
	int index = 0;
	switch (this->uartNo) {
		case 0:
			READ_MSG_UART(0);
			break;
		case 1:
			READ_MSG_UART(1);
			break;
		case 2:
			READ_MSG_UART(2);
			break;
		case 3:
			READ_MSG_UART(3);
			break;
		default:
			break;
	}
	return index;
}			

	
/************************************************************************/
/* Declare 4 instants according to 4 uart modules                       */
/************************************************************************/						
Uart Uart0(0);
Uart Uart1(1);
Uart Uart2(2);
Uart Uart3(3);