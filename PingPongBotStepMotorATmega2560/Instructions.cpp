#include "Instructions.h"
#include "Driving.h"
#include "stddef.h"

char instructionBuffer[64];
typedef unsigned char byte;

bool instructionCheck(char instructionBuffer[]) {
	if (instructionBuffer[0] != 0x05) return false;
	if (instructionBuffer[1] != 0x09) return false;
	return true;
}

Instruction::Instruction(char instructionBuffer[]) {
	this->callback = NULL;
	this->type = (InstructionType)instructionBuffer[2];
	if (instructionCheck(instructionBuffer)) {
		switch (this->type) {
			case EMER_STOP:
				this->callback = [](void* _self, void* _buffer) {
					cutDriverPower();
				};
				break;
			case STOP:
				this->callback = [](void* _self, void* _buffer) {
					stop();
				};
				break;
			case RUN_STRAIGHT:
				this->callback = [](void* _self, void* _buffer) {
					byte* addr = (byte*)_buffer + 3;
					long& distance = *((long*)addr);
					stop();
					goStraight(10 * distance);
				};
				break;
			case ROTATE:
				this->callback = [](void* _self, void* _buffer) {
					byte* addr = (byte*)_buffer + 3;
					float& angle = *((float*)addr);
					stop();
					rotate(angle);
				};
				break;
			case RUN_ARC:
				this->callback = [](void* _self, void* _buffer) {
					byte* buffer = (byte*)_buffer;
					byte& side = *((byte*)(buffer + 3));
					long& radius = *((long*)(buffer + 4));
					float& angle  = *((float*)(buffer + 8));
					stop();
					runArc(side, 10 * radius, angle);
				};
				break;
			case SET_COLL_SP:
				this->callback = [](void* _self, void* _buffer) {
					byte* addr = (byte*)_buffer + 3;
					long& percent = *((long*)addr);
					setCollectorSpeed(percent);
				};
				break;
			case SET_LIGHT:
				this->callback = [](void* _self, void* _buffer) {
					byte* addr = (byte*)_buffer + 3;
					long& percent = *((long*)addr);
					setLightLevel(percent);
				};
				break;
			case BATT_REQ:
				break;
			case BATT_RES:
				break;
			case BATT_WARN:
				break;
			case OBSTACLES_WARN:
				break;
			case UNKNOWN_ERR:
				break;
			default:
				break;
		};
	}
}

void Instruction::execute() {
	if (this->callback != NULL) this->callback((void*)this, (void*)instructionBuffer);
}