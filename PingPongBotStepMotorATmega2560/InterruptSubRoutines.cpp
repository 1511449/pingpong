/*
 * InterruptSubRoutines.cpp
 *
 * Created: 10/11/2019 5:09:00 AM
 *  Author: huynh
 */ 
#include "BitOperations.h"
#include "Driving.h"
#include <avr/interrupt.h>

volatile long leftMotorPulseCount = 0;
volatile long rightMotorPulseCount = 0;

/************************************************************************/
/* ISR for two pulse generators - step motor control pulses             */
/************************************************************************/

ISR(TIMER1_COMPA_vect) {				// Timer for left motor pulse
	IO_WRITE_HIGH(LEFT_MOTOR_PULSE);	// Set HIGH
	leftMotorPulseCount++;				// Count the number of pulse that left motor has raised
	sei();								// Allow global interrupt
}

ISR(TIMER1_COMPB_vect) {				// Timer for left motor pulse
	IO_WRITE_LOW(LEFT_MOTOR_PULSE);		// Set LOW
	if (leftMotorPulseCount >= leftMotorNumberOfPulses)
		TCCR1B &= 0b11111000;			// Stop timer 1 by setting prescaler to 0
	sei();								// Allow global interrupt
}

ISR(TIMER3_COMPA_vect) {				// Timer for right motor pulse
	IO_WRITE_HIGH(RIGHT_MOTOR_PULSE);	// Set HIGH
	rightMotorPulseCount++;				// Count the number of pulse that right motor has raised
	sei();								// Allow global interrupt
}

ISR(TIMER3_COMPB_vect) {				// Timer for right motor pulse
	IO_WRITE_LOW(RIGHT_MOTOR_PULSE);	// Set LOW
	if (rightMotorPulseCount >= rightMotorNumberOfPulses)
		TCCR3B &= 0b11111000;			// Stop timer 3 by setting prescaler to 0
	sei();								// Allow global interrupt
}


ISR(TIMER4_COMPA_vect) {				// Timer for PWM for COLLECTOR_MOTOR
	IO_WRITE_LOW(COLLECTOR_MOTOR_PIN);
}

ISR(TIMER4_OVF_vect) {					// Timer for PWM for COLLECTOR_MOTOR
	IO_WRITE_HIGH(COLLECTOR_MOTOR_PIN);
}

ISR(TIMER5_COMPA_vect) {				// Timer for PWM for LIGHT
	IO_WRITE_LOW(LIGHT_PIN);
}

ISR(TIMER5_OVF_vect) {					// Timer for PWM for LIGHT
	IO_WRITE_HIGH(LIGHT_PIN);
}