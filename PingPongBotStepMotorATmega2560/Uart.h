/*
 * SerialCommunication.h
 *
 * Created: 12/11/2019 12:49:11 AM
 *  Author: huynh
 */ 

#ifndef SERIALCOMMUNICATION_H_
#define SERIALCOMMUNICATION_H_

#define RECV_BUFFER_SIZE		64

#define NO_PARITY				0
#define ODD_PARITY				1
#define EVEN_PARITY				2

#define FOSC_HZ					16000000
#define UBBR_VALUE_NORMAL(baudrate) (round(FOSC_HZ/(16.0 * baudrate) - 1))
#define UBBR_VALUE_DOUBLE_SPEED(baudrate) (round(FOSC_HZ/(8.0 * baudrate) - 1))

class Uart {
	int uartNo;
	char* recvBuffer;
	int recvBufferSize;
public:
	Uart(int _uartNo);
	void begin(long baudrate, int charactersize = 8, unsigned char parity = 0, unsigned char numberOfStopBit = 1, bool doubleSpeed = false);
	void write(char* const buffer, int _numberOfBytes = 0);
	void writeln(char* const buffer, int _numberOfBytes = 0);
	int available();
	char readChar();
	void setMsgRecvBuffer(char* const _recvBuffer, const int _recvBufferSize);
	int readMsg();		// Read uart data and put it to this->recvBuffer
	void setCallBack(void(*_callback)(Uart&));
};

extern Uart Uart0;
extern Uart Uart1;
extern Uart Uart2;
extern Uart Uart3;
#endif /* SERIALCOMMUNICATION_H_ */