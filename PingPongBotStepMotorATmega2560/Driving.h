/*
 * Driving.h
 *
 * Created: 10/11/2019 5:15:26 AM
 *  Author: huynh
 */ 


#ifndef DRIVING_H_
#define DRIVING_H_

/************************************************************************/
/* Mechanical / electrical characteristic								*/
/************************************************************************/
#define DEVICE_CLOCK_FREQUENCY_HZ				16000000
#define MOTOR_PULSE_PER_ROUND					800
#define WHEEL_DIAMETER_MILIMETER				100
#define MAX_ROBOT_SPEED_MILIMETER_PER_SEC		500
#define GEAR_RATIO								0.25	// Wheel's rate = GEAR_RATIO * Motor's rate
#define CYCLE_HIGH_TIME_MICROSECOND				1
#define DISTANCE_BETWEEN_TWO_WHEEL_MILIMETER	392.5
#define LEFT_MOTOR_DIR_REVERSED					true
#define RIGHT_MOTOR_DIR_REVERSED				true

/************************************************************************/
/* Declare IO pins, abide by the following syntax:						*/
/************************************************************************/
//		PIN_NAME				PORT			COMMA			PIN
#define LEFT_MOTOR_PULSE		A				,				0
#define	LEFT_MOTOR_DIR			A				,				2
#define LEFT_MOTOR_EN			A				,				4
#define RIGHT_MOTOR_PULSE		A				,				1
#define	RIGHT_MOTOR_DIR			A				,				3
#define RIGHT_MOTOR_EN			A				,				5
#define COLLECTOR_MOTOR_PIN		E				,				5
/// IMPORTANT: The pin DRIVER_POWER_SOURCE below is used to keep the driver powered on.
//	IMPORTANT: Writing a zero value to this pin cause a power cut at the driver
#define DRIVER_POWER_SOURCE		G				,				5
#define LIGHT_PIN				E				,				4

/**********************************
 * Usage with macros listed bellow:
 *---------------------------------
 * SET_IO_DIR_OUTPUT(X)
 * SET_IO_DIR_INTPUT(X)
 * IO_WRITE_HIGH(X)
 * IO_WRITE_LOW(X)
 * IO_TOGGLE(X)
 *---------------------------------
 * Example:
   #define		A_SPECIFIC_FEATURE_IO_PIN		B, 6
   SET_IO_DIR_OUTPUT(A_SPECIFIC_FEATURE_IO_PIN);
 **********************************/


#include "Uart.h"

extern long leftMotorNumberOfPulses;
extern long rightMotorNumberOfPulses;
extern volatile long leftMotorPulseCount;
extern volatile long rightMotorPulseCount;

void initDriver(Uart& uart);
void setLightLevel(long percent);
void setCollectorSpeed(long percent);
void runLeft(long distanceMilimeter, long speedMilimeterPerSecond);
void runRight(long distanceMilimeter, long speedMilimeterPerSecond);
void stop();
void cutDriverPower();
void goStraight(long _distance);
void rotate(float _angle);
void runArc(unsigned char _side, long _radius, float _angle);

#endif /* DRIVING_H_ */